# -*- coding: utf-8 -*-
"""
Created on Thu May 05 16:56:28 2016

@author: MyoungHoon
"""

import cPickle as pickle
from sklearn.preprocessing import OneHotEncoder
import numpy as np
import copy
import csv
import datetime
import pandas as pd
import scipy.sparse as ssprs


trainFile = "train10k.csv"
testFile = "test5k.csv"
merchantInfoFile = "merchant.csv"
lmDic = {}
budgetDic = {}
global merPopularity
global trLst
global ttLst


def columnAsMat(matrix, i):
    return [[row[i]] for row in matrix]


def columnAsLst(matrix, i):
    return [row[i] for row in matrix]


def storeLstDic(dic, key, data):
    if key in dic:
        lst = dic[key]
        lst.append(data)
    else:
        dic[key] = [data]


def readCsvMat(fileName):
    mat = np.genfromtxt(fileName, delimiter=",", dtype=int)
    lst = mat.tolist()
    return lst, mat


def readCsvMerchantInfo():
    with open(merchantInfoFile, 'r') as f:
        reader = csv.reader(f, delimiter=',', quotechar='"')
        for row in reader:
            mer, budget, locStr = int(row[0]), int(row[1]), row[2]
            locLst = [int(x) for x in locStr.split(':')]
            for loc in locLst:
                storeLstDic(lmDic, loc, mer)
            budgetDic[mer] = budget


def oneHotEncoding(field):
    enc = OneHotEncoder()
    enc.fit(field)
    return enc


def preprocessing():
    global trLst, ttLst, merPopularity

    trLst, _trMat = readCsvMat(trainFile)
    ttLst, _ttMat = readCsvMat(testFile)
    readCsvMerchantInfo()

    # train set
    sortedTrLst = sorted(trLst, key=lambda x: x[3])

    trUId = columnAsMat(sortedTrLst, 0)
    trMId = columnAsMat(sortedTrLst, 1)
    trLId = columnAsMat(sortedTrLst, 2)
    npTrMId = np.array(trMId)

    trDate = columnAsLst(sortedTrLst, 3)
    dateLst = list(set(trDate))
    dateLst.sort()

    ttUId = columnAsMat(ttLst, 0)
    ttLId = columnAsMat(ttLst, 1)

    uId = trUId + ttUId
    lId = trLId + ttLId

    mer, cnt = np.unique(trMId, return_counts=True)
    merCnt = np.asarray((mer, cnt)).T
    merPopularity = sorted(merCnt.tolist(), key=lambda x: -x[1])

    uEnc = oneHotEncoding(uId)
    lEnc = oneHotEncoding(lId)
    mEnc = oneHotEncoding(trMId)

    trUL = np.concatenate((trUId, trLId), axis=1)

    b = np.ascontiguousarray(trUL).view(np.dtype((np.void, trUL.dtype.itemsize * trUL.shape[1])))
    _, idx = np.unique(b, return_index=True)
    uniUL = copy.deepcopy(trUL[idx])

    for uniRow in uniUL:
        tmp = np.where(np.all(uniRow == trUL, axis=1))
        indice = tmp[0]

        visitM = npTrMId[indice]
        ohTrMId = mEnc.transform(visitM).toarray()
        # print indice, visitM, ohTrMId
        tmp = np.cumsum(ohTrMId, axis=0)
        cumVec = tmp.tolist()[-1]
        visitVec = [1 if d > 0.0 else 0 for d in cumVec]
        print uniRow, visitVec, cumVec


def predict(x):
    user, loc = x
    lmLst = lmDic[loc]
    merLst = [i for i, cnt in merPopularity if i in lmLst]
    yLst = merLst[:10]
    y = None
    if len(yLst) > 0:
        # print yLst
        y = ":".join("{0}".format(n) for n in yLst)
    return y


def predictSubmission():
    dtStr = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d_%H%M')
    with open(dtStr + ".csv", 'w') as f:
        for x in ttLst:
            y = predict(x)
            rowStrLst = [str(i) for i in x]
            if y is not None:
                rowStrLst.append(y)
            rowStr = ",".join(rowStrLst)
            f.write(rowStr + "\n")


def getWindowMat(dateLst, st, ed, trMat):
    pass





if __name__ == '__main__':
    preprocessing()
    
    #import numpy, scipy.sparse
    #n = 100000
    #x = (numpy.random.rand(n) * 2).astype(int).astype(float)  # 50% sparse vector
    #x_csr = scipy.sparse.csr_matrix(x)
    #x_dok = scipy.sparse.dok_matrix(x.reshape(x_csr.shape))

    














